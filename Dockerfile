FROM python:3.8
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
WORKDIR /app
RUN pip install poetry
ADD . /app
RUN poetry config virtualenvs.create false
RUN apt-get -y update && apt-get -y upgrade && apt-get install -y --no-install-recommends ffmpeg
RUN poetry install
CMD ["/app/start.sh", "--run"]
