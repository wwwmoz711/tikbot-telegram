emoji<2.0.0,>=1.2.0
jsonpath-ng<2.0.0,>=1.5.2
python-dotenv<0.16.0,>=0.15.0
python-telegram-bot<14.0,>=13.2
redis<4.0.0,>=3.5.3
requests<3.0.0,>=2.25.1
sentry-sdk<0.20.0,>=0.19.5
