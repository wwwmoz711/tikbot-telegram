import requests
import validators
from sentry_sdk import capture_exception
from telegram import InlineQueryResultVideo
from telegram.constants import ParseMode
from telegram.ext import ContextTypes

from tikbot_telegram.util import build_caption, upload_tiktok


async def inline_handler(update, context: ContextTypes.DEFAULT_TYPE):
    from tikbot_telegram.main import logger

    query = await update.inline_query.query.split(" ")[0]
    logger.info("Received query: %s " % query)
    if validators.url(query):
        try:
            video_data_res = requests.get(
                "https://api.douyin.wtf/api", params={"url": query}
            )
            if not video_data_res.ok:
                return
            data = video_data_res.json()
            if data.get("type") != "video":
                return
            video_url = upload_tiktok(data)

            logger.info(
                "Processed inline query for %s, video URL: %s" % (query, video_url)
            )

            caption = build_caption(data, "")

            results = [
                InlineQueryResultVideo(
                    id=data.get("video_aweme_id"),
                    video_url=video_url,
                    mime_type="video/mp4",
                    caption=caption,
                    title="Send this video",
                    description=data.get("video_title"),
                    thumb_url=data.get("video_origin_cover"),
                    parse_mode=ParseMode.HTML,
                )
            ]
            await update.inline_query.answer(results)
        except Exception as e:
            capture_exception(e)
            logger.info("Failed to process inline query %s: %s" % (query, e))
            return
