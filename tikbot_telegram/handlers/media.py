import json
import math
import multiprocessing
import shutil
from io import BytesIO
from tempfile import NamedTemporaryFile

import requests
from pydub import AudioSegment
from sentry_sdk import add_breadcrumb, start_transaction
from telegram import Update
from telegram.constants import ParseMode, ChatAction
from telegram.error import BadRequest
from telegram.ext import CallbackContext

from tikbot_telegram.util import build_caption, download_media


async def process_media(
    update: Update, url: str, text: str, redis_instance, context: CallbackContext
):
    from tikbot_telegram.main import logger

    message = update.effective_message
    add_breadcrumb(
        category="telegram",
        message="Processing video %s, requested by %s" % (url, message.from_user.name),
        level="info",
    )

    video_data_res = requests.get("https://api.douyin.wtf/api", params={"url": url})

    item_infos = video_data_res.json()
    if not video_data_res.ok or item_infos.get("status") == "failed":
        logger.error(
            "Failed to request from TikBot API: %s" % video_data_res.status_code
        )
        add_breadcrumb(
            category="tikbot",
            message="Failed to request from TikTok API: %s, %s"
            % (video_data_res.status_code, video_data_res.text),
            level="error",
        )
        await message.reply_html(
            f"Could not download video \U0001f613, are you sure this is a public TikTok <b>video</b>? \U0001f97a"
        )
        video_data_res.raise_for_status()
        return

    if item_infos.get("type") == "video":
        if video_id := item_infos.get("aweme_id"):
            file_id = redis_instance.get(video_id)
            if file_id:
                with start_transaction(op="cache", description="Return cached video"):
                    logger.info("Returning cached video for %s" % url)
                    try:
                        reply = await message.reply_video(
                            video=file_id.decode("utf-8"),
                            disable_notification=True,
                            caption=build_caption(item_infos, text, message),
                            parse_mode=ParseMode.HTML,
                        )

                        try:
                            await message.delete()
                        except BadRequest:
                            pass

                        return reply
                    except Exception as e:
                        logger.warning("Failed to deliver from cache: %s" % e)
                        redis_instance.delete(file_id)
        # Initialize a temporary file in-memory for storing and then uploading the video
        with start_transaction(
            op="download_video", description="Download and send video"
        ):
            with NamedTemporaryFile(suffix=".mp4") as f:
                await message.chat.send_action(action=ChatAction.UPLOAD_VIDEO)
                # Get the video URL
                video_url = item_infos.get("video_data", {}).get("nwm_video_url_HQ")
                if len(video_url) == 0:
                    logger.error(
                        "Failed to find videoUrl in video meta: %s"
                        % json.dumps(item_infos)
                    )
                    add_breadcrumb(
                        category="tiktok",
                        message="Failed to find videoUrl in video meta",
                        level="error",
                    )
                    await message.reply_html(
                        f"Could not download video \U0001f613, TikTok gave a bad video meta response \U0001f97a"
                    )
                    raise
                with requests.get(video_url, stream=True) as r:
                    if not r.ok:
                        logger.debug(f"Failed to download video {item_infos}")
                        add_breadcrumb(
                            category="tiktok",
                            message="Failed to download video from TikTok: %s"
                            % r.status_code,
                            level="error",
                        )
                        await message.reply_html(
                            f"Could not download video \U0001f613, TikTok gave a bad response \U0001f97a ({r.status_code})"
                        )
                        return
                    shutil.copyfileobj(r.raw, f)

                logger.info("Processed video %s" % url)

                reply = await message.reply_video(
                    video=open(f.name, "rb"),
                    disable_notification=True,
                    caption=build_caption(item_infos, text, message),
                    parse_mode=ParseMode.HTML,
                )

                try:
                    await message.delete()
                except BadRequest:
                    pass

                redis_instance.set(item_infos.get("aweme_id"), reply.video.file_id)

                return reply
    elif item_infos.get("type") == "image":
        await message.chat.send_action(action=ChatAction.UPLOAD_PHOTO)
        with start_transaction(
            op="download_album", description="Download and send album"
        ):
            media_files = item_infos.get("image_data", {}).get(
                "no_watermark_image_list", []
            )
            try:
                with multiprocessing.Pool() as pool:
                    input_files = pool.map(download_media, media_files)
            except Exception as e:
                await message.reply_html(
                    f"Could not download album \U0001f613, TikTok gave a bad response \U0001f97a"
                )
                return

            chunks = [input_files[x : x + 10] for x in range(0, len(input_files), 10)]

            chunks[-1][-1].caption = build_caption(item_infos, text, message)
            chunks[-1][-1].parse_mode = ParseMode.HTML

            media_messages = [await message.reply_media_group(media=chunk) for chunk in chunks]

            try:
                await message.delete()
            except BadRequest:
                pass

            # Process audio
            try:
                with NamedTemporaryFile() as f:
                    AudioSegment.from_file(
                        BytesIO(
                            requests.get(
                                item_infos.get("music", {})
                                .get("play_url", {})
                                .get("uri")
                            ).content
                        )
                    ).export(f)

                    await media_messages[-1][-1].reply_audio(
                        f,
                        filename="%s.mp3" % item_infos.get("music", {}).get("title"),
                        performer=item_infos.get("music", {}).get("author"),
                        title=item_infos.get("music", {}).get("title"),
                    )
            except requests.exceptions.RequestException:
                pass

            logger.info("Processed album %s" % url)

            return
    else:
        logger.error("URL is not a video: %s" % json.dumps(item_infos))
        add_breadcrumb(
            category="tiktok",
            message="URL is not a video",
            level="error",
        )
        await message.reply_html(
            f"Could not download \U0001f613, this is not a video \U0001f97a"
        )
