import html
import shutil
from tempfile import NamedTemporaryFile

import requests
from PIL import Image
from google.cloud import storage
from sentry_sdk import add_breadcrumb
from telegram import Chat, InputMediaPhoto, Message
from telegram.constants import ParseMode


def truncate(s, x=3000):
    return s[:x] + (s[x:] and "..")


def build_caption(item_infos, text, message: Message = None):
    author = '<a href="https://tiktok.com/@%s">@%s</a>'
    author = author % (
        item_infos.get("author", {}).get("uid"),
        html.escape(item_infos.get("author", {}).get("nickname")),
    )

    video_caption = item_infos.get("author", {}).get("signature", "")
    if "#" in video_caption:
        video_caption = html.escape(video_caption.split("#")[0])
    likes = item_infos.get("statistics", {}).get("digg_count", 0)
    comments = item_infos.get("statistics", {}).get("comment_count", 0)
    plays = item_infos.get("statistics", {}).get("play_count", 0)
    video_caption = html.escape(video_caption.strip())

    user_part = ""
    if message is not None:
        if message.chat.type is not Chat.PRIVATE and message.from_user is not None:
            user_part = (
                f"{message.from_user.name} "
                if not text
                else f"{message.from_user.name}: {text}\n "
            )

    return (
        user_part
        + "\n%s" % author
        + (
            f'\n<a href="{item_infos.get("url")}">{video_caption}</a>\n'
            if len(video_caption.replace(" ", "")) > 0
            else f'\n<a href="{item_infos.get("url")}">TikTok</a>\n'
        )
        + (
            f"{int(likes):,} \u2764\ufe0f {int(comments):,} \U0001f4ad {int(plays):,} \u23ef"
        )
    )


def upload_tiktok(video_meta):
    from tikbot_telegram.main import bucket_name

    storage_client = storage.Client()

    bucket = storage_client.bucket(bucket_name)

    blob = bucket.blob("%s.mp4" % video_meta.get("aweme_id"))

    with NamedTemporaryFile(suffix=".mp4") as f:
        with requests.get(
            video_meta.get("video_data", {}).get("nwm_video_url_HQ"),
            stream=True,
            headers=video_meta.get("headers", {}),
        ) as r:
            r.raise_for_status()
            shutil.copyfileobj(r.raw, f)
            f.seek(0)
            blob.upload_from_file(f)

    blob.make_public()
    return blob.media_link


def download_media(image_url):
    from tikbot_telegram.main import logger

    with requests.get(image_url, stream=True, verify=False) as r:
        if not r.ok:
            logger.info(f"Failed to download image {image_url}: {r.status_code}")
            add_breadcrumb(
                category="tiktok",
                message="Failed to download image from TikTok: %s" % image_url,
                level="error",
            )
            raise Exception("Failed to download image from TikTok")
        with NamedTemporaryFile(suffix=".jpg") as f:
            Image.open(r.raw).save(f.name, "JPEG")
            return InputMediaPhoto(
                open(f.name, "rb"),
                parse_mode=ParseMode.HTML,
            )
