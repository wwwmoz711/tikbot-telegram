import logging
import os
import re
import signal
from typing import Optional

import sentry_sdk
from dotenv import load_dotenv, find_dotenv
from telegram import MessageEntity
from telegram.ext import (
    Updater,
    MessageHandler,
    CommandHandler,
    InlineQueryHandler,
    Application,
    AIORateLimiter,
    filters,
    CallbackContext,
)
from urllib3.exceptions import InsecureRequestWarning

from tikbot_telegram.handlers.inline import inline_handler
from tikbot_telegram.handlers.message_handler import tiktok_handler
from tikbot_telegram.handlers.start import start_handler
import warnings

warnings.filterwarnings(
    "ignore", category=InsecureRequestWarning
)  # urllib3 is currently broken, let's not spam the logs..

load_dotenv(find_dotenv())

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger("TikBot")

if os.getenv("SENTRY_DSN"):
    sentry_sdk.init(dsn=os.getenv("SENTRY_DSN"), traces_sample_rate=0.2)

link_regex = re.compile(r"^https:\/\/(www|m|vm)\.tiktok\.com\/.+$")

os.environ.setdefault("REDIS_HOST", "localhost")
os.environ.setdefault("REDIS_PORT", "6379")
os.environ.setdefault("REDIS_DB", "0")

try:
    assert "GOOGLE_APPLICATION_CREDENTIALS" in os.environ.keys()
    bucket_name = os.environ["BUCKET_NAME"]
except (AssertionError, KeyError):
    logger.critical(
        "Please set the GOOGLE_APPLICATION_CREDENTIALS and BUCKET_NAME environment variable"
    )
    exit(1)


async def error_handler(update: Optional[object], context: CallbackContext) -> None:
    """A callback function that is called when the bot receives a message."""
    logger.fatal(msg="Exception while handling an update:", exc_info=context.error)
    os.kill(os.getpid(), signal.SIGINT)


if __name__ == "__main__":
    application = (
        Application.builder()
        .token(os.getenv("TELEGRAM_TOKEN"))
        .connect_timeout(10)
        .rate_limiter(AIORateLimiter())
        .build()
    )
    handler = MessageHandler(
        (filters.Entity(MessageEntity.URL) | filters.Entity(MessageEntity.TEXT_LINK)),
        tiktok_handler,
    )
    application.add_handler(handler)
    application.add_handler(CommandHandler("start", start_handler))
    application.add_handler(InlineQueryHandler(inline_handler))
    application.add_error_handler(error_handler)
    logger.info("TikBot booted")
    application.run_polling(drop_pending_updates=True)
